conf
====

Config files for i3

Dependencies

* SF UI Disaplay font _(package included)_
* xscreensaver
* bumblebee-status
* [Rofi](https://davedavenport.github.io/rofi/)
